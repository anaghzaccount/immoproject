import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import{BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FlexLayoutModule} from '@angular/flex-layout';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { FormsModule,ReactiveFormsModule } from '@angular/forms'; 
import { MatOptionModule, MatSelectModule, MatIconModule, MatNativeDateModule, MatCardModule, MatListModule, MatProgressSpinnerModule, MatButtonModule } from '@angular/material';
import {AppRoutingModule} from './app-routing/app-routing.module'
import {RestangularModule} from 'ngx-restangular';
import {HttpClientModule} from '@angular/common/http';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { AddtaskComponent } from './addtask/addtask.component';
import { ListtaskComponent } from './listtask/listtask.component';
import { EdittaskComponent } from './edittask/edittask.component';

import { RestangularConfigFactory } from './shared/restConfig';
import { baseURL } from './shared/baseurl';

import {TaskService} from './services/task.service';
import {ProcessHTTPMsgService} from './services/process-httpmsg.service';
import { HeaderComponent } from './header/header.component';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AddtaskComponent,
    ListtaskComponent,
    EdittaskComponent,
    HeaderComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FlexLayoutModule,
    MatToolbarModule,
    MatIconModule,
    MatCardModule,
    MatListModule,
    MatButtonModule,
    MatProgressSpinnerModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatFormFieldModule,
    MatInputModule,
    FormsModule,
    ReactiveFormsModule,
    MatOptionModule,
    MatSelectModule,
    AppRoutingModule,
    HttpClientModule,
    RestangularModule.forRoot(RestangularConfigFactory)
  ],
  providers: [
    TaskService,
    ProcessHTTPMsgService,
    {provide:'BaseURL',useValue:baseURL}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
