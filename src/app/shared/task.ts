export class Task {
    name: string;
    status: string;
    description: string;
    date: string;
};

export const Status = ['Open','In-Progress','Closed'];