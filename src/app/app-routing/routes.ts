import {Routes} from '@angular/router';

import {AddtaskComponent} from '../addtask/addtask.component';
import {ListtaskComponent} from '../listtask/listtask.component';
import {HomeComponent} from '../home/home.component';
import {EdittaskComponent} from '../edittask/edittask.component';

export const routes: Routes = [
    {path: 'home', component: HomeComponent},
    {path: 'addtask', component: AddtaskComponent },
    {path: 'listtask', component: ListtaskComponent},
    {path: 'edittask/:id',component:EdittaskComponent},
    {path: '', redirectTo:'/home', pathMatch:'full'},
];
