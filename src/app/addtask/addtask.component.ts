import { Component, OnInit } from '@angular/core';
import {FormBuilder,FormGroup,FormGroupDirective,Validators} from '@angular/forms';
import {TaskService} from '../services/task.service';
import {Task, Status} from '../shared/task';
import {expand, flyInOut} from '../animations/app.animation';

@Component({
  selector: 'app-addtask',
  templateUrl: './addtask.component.html',
  styleUrls: ['./addtask.component.scss'],
  animations: [expand(),flyInOut()]
})
export class AddtaskComponent implements OnInit {

  taskForm: FormGroup;
  task: Task;
  status = Status;
  taskcopy: Task;
  errMess: string;
  flag: boolean = false;
  copyflag:boolean = false;

  formErrors = {
    'name':'',
    'description':'',
    'date':''
  };

  validationMessages = {
    'name':{
      'required':'Task Name is required',
      'minlength':'Task Name cannot be shorter than 2 characters',
      'maxlength':'Task Name cannot be longer than 25 characters'
    },
    'description':{
      'required':'Description is required'

    },
    'date':{
      'required':'Date is required',
      'pattern':'Number format is invalid'
    }
  };

  constructor(private fb:FormBuilder, private taskService: TaskService) {
    this.createForm();
   }

  ngOnInit() {
  }

  createForm(): void
  {
    this.taskForm = this.fb.group({
      name:['',[Validators.required, Validators.minLength(2), Validators.maxLength(25)]],
      status:'Open',
      description:['',Validators.required],
      date:['',Validators.required]
    });

    this.taskForm.valueChanges
    .subscribe(data=>this.onValueChanged(data));
  }

  onValueChanged(data?: any)
  {
    if(!this.taskForm) {return;}
    const form = this.taskForm;
    for (const field in this.formErrors)
    {
      //Clear form errors, if any
      this.formErrors[field]='';
      const control = form.get(field);
      if(control && control.dirty && !control.valid)
      {
        const messages = this.validationMessages[field];
        for(const key in control.errors)
        {
          this.formErrors[field]+=messages[key] + ' ';
        }
      }
    }
  }

  onSubmit(formDirective: FormGroupDirective)
  {
    this.task=this.taskForm.value;
    this.flag=!this.flag;
    this.taskService.AddTask(this.task)
    .subscribe(
      task=>
      {
        this.task=task;
        this.taskcopy = task;
        this.flag=!this.flag;
        this.copyflag=!this.copyflag;
        setTimeout((function(){this.copyflag=!this.copyflag; console.log("after timeout copyflag:" + this.copyflag)}).bind(this),5000); 
        console.log("Task "+JSON.stringify(task));}, errmess=>{ this.errMess = <any>errmess; console.log("error "+errmess);});;
    console.log(this.task);
    formDirective.resetForm();
    this.taskForm.reset({
      name:'',
      status:'Open',
      description:'',
      date:''
    });
  }

  onReset(formDirective:FormGroupDirective)
  {
    formDirective.resetForm();
    this.taskForm.reset({
      status:'Open'
    });
  }

}
