import { Component, OnInit } from '@angular/core';
import {Params, ActivatedRoute, Router} from '@angular/router';
import {FormBuilder,FormGroup,FormGroupDirective,Validators} from '@angular/forms';
import {switchMap, tap} from 'rxjs/operators';

import {TaskService} from '../services/task.service';
import {Task, Status} from '../shared/task';
import { flyInOut } from '../animations/app.animation';

@Component({
  selector: 'app-edittask',
  templateUrl: './edittask.component.html',
  styleUrls: ['./edittask.component.scss'],
  animations:[flyInOut()]
})
export class EdittaskComponent implements OnInit {

  edittaskForm: FormGroup;
  task: Task;
  taskcopy: Task;
  status = Status;
  errMess: string;
  tempid: any;
  

  formErrors = {
    'name':'',
    'description':'',
    'date':''
  };

  validationMessages = {
    'name':{
      'required':'Task Name is required',
      'minlength':'Task Name cannot be shorter than 2 characters',
      'maxlength':'Task Name cannot be longer than 25 characters'
    },
    'description':{
      'required':'Description is required'

    },
    'date':{
      'required':'Date is required',
      'pattern':'Number format is invalid'
    }
  };

  constructor(private fb:FormBuilder, private route: ActivatedRoute, private taskService: TaskService, private router:Router ) {
    this.createForm();
   }

  ngOnInit() {
    this.route.params
    .pipe(switchMap((params: Params) => { return this.taskService.getTask(+params['id'])}))
    .pipe(tap(user => {this.edittaskForm.patchValue(user); this.tempid=user}))
    .subscribe(task=>{this.task = task; this.taskcopy = task;}, errmess=> this.errMess = <any>errmess);

  }
  
  createForm(): void
  {
    this.edittaskForm = this.fb.group({
      name:['',[Validators.required, Validators.minLength(2)]],
      status:'Open',
      description:['',Validators.required],
      date:['',Validators.required]
    });

    this.edittaskForm.valueChanges
    .subscribe(data=>this.onValueChanged(data));
  }

  onValueChanged(data?: any)
  {
    if(!this.edittaskForm) {return;}
    const form = this.edittaskForm;
    for (const field in this.formErrors)
    {
      //Clear form errors, if any
      this.formErrors[field]='';
      const control = form.get(field);
      if(control && control.dirty && !control.valid)
      {
        const messages = this.validationMessages[field];
        for(const key in control.errors)
        {
          this.formErrors[field]+=messages[key] + ' ';
        }
      }
    }
  }

  onSubmit(formDirective: FormGroupDirective)
  {
    let updatearray = {
      id: this.tempid.id,
      name: this.edittaskForm.get('name').value,
      status:this.edittaskForm.get('status').value,
      description: this.edittaskForm.get('description').value,
      date: this.edittaskForm.get('date').value
    }

    this.route.params
    .pipe(switchMap((params: Params) => { return this.taskService.PostTask(+params['id'],updatearray)}))
    .subscribe(task=>{
      this.taskcopy = task; 
      console.log("Task "+task);
      this.router.navigate(['/listtask']);}, errmess=>{ this.errMess = <any>errmess; console.log("error "+errmess);});
    
   
    
    
  }


}
