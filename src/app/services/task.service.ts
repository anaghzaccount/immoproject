import { Injectable } from '@angular/core';
import {Task} from '../shared/task';
import {baseURL} from '../shared/baseurl';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';

import {ProcessHTTPMsgService} from './process-httpmsg.service';

@Injectable({
  providedIn: 'root'
})
export class TaskService {

  constructor(private http:HttpClient, private processHTTPMsgService: ProcessHTTPMsgService) { }

  getTasks(): Observable<Task[]>{

    return this.http.get<Task[]>(baseURL + 'tasks')
    .pipe(catchError(this.processHTTPMsgService.handleError));
  }

  getTask(id: number): Observable<Task> {

    return this.http.get<Task>(baseURL + 'tasks/' + id)
    .pipe(catchError(this.processHTTPMsgService.handleError));

  }

  AddTask(task:Task):Observable<Task>{

    return this.http.post<Task>(baseURL + 'tasks', task)
    .pipe(catchError(this.processHTTPMsgService.handleError));
  }


  PostTask(id:number,task:Task):Observable<Task>{
    
    return this.http.put<Task>(baseURL+'tasks/' + id, task)
    .pipe(catchError(this.processHTTPMsgService.handleError));
    
  }

  DeleteTask(id:number):Observable<{}>{
    return this.http.delete(baseURL + 'tasks/' + id)
    .pipe(catchError(this.processHTTPMsgService.handleError));
  }

}
