import { Component, OnInit } from '@angular/core';
import {flyInOut} from '../animations/app.animation';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  animations:[flyInOut()]
})
export class HomeComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
