import { Component, OnInit } from '@angular/core';

import {TaskService} from '../services/task.service';
import {Task} from '../shared/task';
import {flyInOut} from '../animations/app.animation';

@Component({
  selector: 'app-listtask',
  templateUrl: './listtask.component.html',
  styleUrls: ['./listtask.component.scss'],
  animations: [flyInOut()]
})
export class ListtaskComponent implements OnInit {

  tasks: Task[];
  errMess: string;

  constructor(private taskService:TaskService) { }

  ngOnInit() {
    this.taskService.getTasks()
    .subscribe(tasks=>this.tasks = tasks, errmess=>this.errMess=errmess);
  }

  deleteconfirm(id)
  {
    console.log("Inside delete! "+ id);
    if(confirm("Are you sure? This is an irrevesible process"))
    {
      this.taskService.DeleteTask(id)
      .subscribe(()=>{
        window.location.reload();
      },errmess=>this.errMess=errmess);
    }
  }

}
