# Immoproject

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.0.5.

## Dependencies

Run `npm install` to install all dependencies
Install json-server by executing `npm install json-server --save` to install json server globally. Since the project was only about front end, json server is used to emulate a REST API Server. There is a folder `json-server` inside the project folder. Navigate to that folder and run `json-server --watch db.json -d=2000` to run the json server. Keep this window running. Make sure the the json-server is running at port 3000 (-d=2000 is used to introduce a delay of 2secs to emulate the asynchronous behaviour). Incase it is running on a different port number, update the same in `app/shared/baseurl.ts`

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

